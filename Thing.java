class Thing{
    private String name;
    private int volume;
    public Thing(String name){
        this.name = name;
        this.volume = 0;
    }
    public Thing(int volume) {
        this.name = " ";
        this.volume = volume;
    }
    public Thing(String name, int volume) {
        this.name = name;
        this.volume = volume;
    }

    public String getNomThing(){
        return this.name;

    }
    public int volume() {
        return this.volume;
    }

    public void setName(String name){
        this.name = name;
    }

    public boolean hasName (String nom){
        return this.name.equals(nom);
    }
}
