import java.util.ArrayList;
import java.util.List;

class Box{
    private ArrayList<Thing> contents;
    private boolean ouverture;
    private int capacite;

    public Box(){
        this.contents = new ArrayList<Thing>();
        this.ouverture = true;
        this.capacite = -1;
    }

    public Box(boolean ouverture, int capacite){
        this.contents = new ArrayList<Thing>();
        this.ouverture = ouverture;
        this.capacite = capacite;
    }
    
    public void add(String truc) {
        this.contents.add(new Thing(truc));
    }

    public boolean estDedans(String mot) {
        for (Thing obj : this.contents) {
            if(obj.hasName(mot)) {
                return true;
            }
        }
        return false;
    }

    public boolean isOpen(){
        return this.ouverture;
    }

    public void close(){
        this.ouverture = false;
    }

    public void open(){
        this.ouverture = true;
    }

    public String actionLook(){
        String res = "La boîte contient: ";
        if (this.ouverture){
            for (Thing obj : this.contents){
                res = res+obj.getNomThing()+"| ";
            }
       }
        else{
            res = "La boîte est fermée";
       }
       return res;
    }

    public void setCapacity(int capaciteBoite){
        this.capacite = capaciteBoite;
    }

    public int capacity(){
        return this.capacite;
    }

    public boolean hasRoomFor(Thing objet) {
        if (this.capacite < objet.volume()) {
            return false;
        }
        return true;

    }

    public void actionAdd(Thing objet) throws PlaceException, CloseException{
        if (this.isOpen()){
            if (hasRoomFor(objet)){
                this.contents.add(objet);
                this.capacite = this.capacite - objet.volume(); 
            }
            else{
                throw new PlaceException();
            }
        }
        else{
            throw new CloseException();
        }
    }

    public Thing find(String objet) throws VideException, CloseException {
        if (this.isOpen()) {
            Thing obj = null;
            for(Thing thing : this.contents) {
                if (thing.hasName(objet)) {
                    obj = thing;
                }
            }
            if(obj == null) {
                throw new VideException();
            }
            return obj;
        }
        else{
            throw new CloseException();
        }
    }

}

    